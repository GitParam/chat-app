 var socket = io();
 var typing = false;
 var timeout = undefined;
 var color = '#';
 var keyPressObject = {};
 $(() => {

     /*load all message for a session*/
     getMessages();
     /*generate random color on page render*/
     var letters = '0123456789ABCDEF';

     for (var i = 0; i < 6; i++) {
         color += letters[Math.floor(Math.random() * 16)];
     }

     /*event handler for "send" button*/
     $("#btnSend").on('click', (event) => {
         event.preventDefault();
         if ($('#message').val()) {
             var message = {
                 user: color,
                 message: $('#message').val()
             };
             $('#message').val('');
             /*post a message*/
             postMessages(message);
         }
     });

     $("#message").keydown(function () {
         onKeyDownNotEnter();
     });

     $(".messagesDiv").on("click", ".actualMessageText .fa-heart", function () {
         var targetElement = {
             id: $(this).attr("id"),
             emoji: "love"
         };
         socket.emit('messageReaction_req', targetElement);
     });

     $(".messagesDiv").on("click", ".actualMessageText .fa-thumbs-up", function () {
         var targetElement = {
             id: $(this).attr("id"),
             emoji: "like"
         };
         socket.emit('messageReaction_req', targetElement);
     });

 });

 /*socket event listeners*/
 socket.on('message', appendMessage);
 socket.on('userConnected', displayConnectedUsers);
 socket.on('userDisconnected', displayConnectedUsers);
 socket.on('typingMessageStatus', typingMessage);
 socket.on('messageReaction_response', messageReaction_response) 

 function messageReaction_response(targetElement) {
     if(targetElement.emoji=="love"){
       $("#" + targetElement.id).css('color', 'red').addClass("floatMe");   
     } else{
       $("#" + targetElement.id).css('color', '#337ab7').addClass("floatMe");          
     }
 }


 function typingMessage(keyPressObject) {
     if ((keyPressObject.typing) && (keyPressObject.color != color)) {
         $(".isUserTypingDiv").css('visibility', 'visible');
     } else {
         $(".isUserTypingDiv").css('visibility', 'hidden');
     }
 }

 function displayConnectedUsers(connectedUsers) {
     var targetElement = $('#usersConnected');
     targetElement.html("<i class='fa fa-users'></i> : " + connectedUsers).addClass("blinkMe");

     setTimeout(function () {
         targetElement.removeClass("blinkMe");
     }, 3000);

 }

 /*function to append a message*/
 function appendMessage(messages) {
     $(".messagesDiv").append(
         `<div class="media newMessage">
              <div class="media-left media-top">
                <i class="fa fa-user-secret fa-2x" style="color:${messages.user}"></i>
              </div>
              <div class="media-body actualMessageText">
                ${messages.message}
                <br/>
                <i class="fa fa-heart" id="${$.guid++}_heart"></i>
                <i class="fa fa-thumbs-up" id="${$.guid++}_thumbsUp"></i>
              </div>
            </div>`
     )

     scrollToBottomOfChat();
     PlaySound("playSound");
 }

 /*function to load all messages for a session*/
 function getMessages() {
     $.get('http://10.11.22.70:3000/messages', (data) => {
         data.forEach(appendMessage);
     })
 }

 /*function to post a message*/
 function postMessages(message) {
     $.post('http://10.11.22.70:3000/messages', message)
 }

 /*function to scroll to bottom of chat on receiving a new message*/
 function scrollToBottomOfChat() {
     var height = 0;
     $('.messagesDiv .newMessage div').each(function (i, value) {
         height += parseInt($(this).height()) + 30;
     });

     height += '';

     $('.messagesDiv').animate({
         scrollTop: height
     }, 20);
 }

 /*function to play sound*/
 function PlaySound(soundObj) {
     var sound = document.getElementById(soundObj);
     sound.play();
 }

 /*funtion to hide "user is typing" message*/
 function timeoutFunction() {
     keyPressObject = {
         typing: false,
         color: color
     };
     socket.emit('noLongerTypingMessage', keyPressObject);
 }

 /*function to show " user is typing" message*/
 function onKeyDownNotEnter() {
     if (typing == false) {
         keyPressObject = {
             typing: true,
             color: color
         };
         socket.emit('typingMessage', keyPressObject);
         timeout = setTimeout(timeoutFunction, 2500);
     } else {
         clearTimeout(timeout);
         timeout = setTimeout(timeoutFunction, 2500);
     }

 }
