var express= require('express');
var bodyParser= require('body-parser');

var app= express();
var http= require('http').Server(app);
var io= require('socket.io')(http);


app.use(express.static(__dirname));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));


var connectedUsers=0;
var messages=[]

app.get('/messages',(req,res)=>{
    res.send(messages);
})

app.post('/messages',(req,res)=>{
    messages.push(req.body);
    io.emit('message',req.body);
    res.sendStatus(200);
})

io.on('connection',(socket)=>{
    connectedUsers++;
    console.log("users connected"+connectedUsers);
    io.emit('userConnected',connectedUsers);
    
    socket.on('disconnect', () => {
        connectedUsers--;
        console.log("users disconnected"+connectedUsers);
        io.emit('userDisconnected',connectedUsers);
    });
    
    socket.on('typingMessage', (keyPressObject) => { 
        console.log("Server typingMessage");
        io.emit('typingMessageStatus',keyPressObject);
    });
    
    socket.on('noLongerTypingMessage', (keyPressObject) => { 
        console.log("server noLongerTypingMessage");
        io.emit('typingMessageStatus',keyPressObject);
    });
    
    socket.on('messageReaction_req', (targetElement) => { 
        console.log("server targetElement"+targetElement.id+"---"+targetElement.emoji);
        io.emit('messageReaction_response',targetElement);
    }); 
    
}) 
 

http.listen(3000);